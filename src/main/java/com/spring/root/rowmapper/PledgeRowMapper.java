package com.spring.root.rowmapper;

/**
 * Created by Kamil on 01/12/2016.
 */
import java.sql.ResultSet;
import java.sql.SQLException;

import com.spring.root.domain.Pledge;
import org.springframework.jdbc.core.RowMapper;

public class PledgeRowMapper implements RowMapper<Pledge> {

    @Override
    public Pledge mapRow(ResultSet rs, int index) throws SQLException {

        Pledge pledge = new Pledge();

        pledge.setPledgeID(rs.getInt("pledgeId"));
        pledge.setAmount(rs.getDouble("amount"));

        //pledge.setUserId(rs.getInt("user_id");
        //pledge.setProjecId(rs.getInt("project_id");


        return pledge;
    }

}
