package com.spring.root.rowmapper;

import com.spring.root.domain.Project;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by seanc on 23/11/2016.
 */
public class ProjectRowMapper implements RowMapper<Project> {
    @Override
    public Project mapRow(ResultSet rs, int index) throws SQLException {
        Project project = new Project();
        project.setProjectID(rs.getInt("projectID"));
        project.setUserID(rs.getInt("userID"));
        project.setProjectName(rs.getString("projectName"));
        project.setDescription(rs.getString("description"));
        project.setGoalAmount(rs.getInt("goalAmount"));
        project.setTimeLimit(rs.getInt("timeLimit"));
        return project;
    }
}
