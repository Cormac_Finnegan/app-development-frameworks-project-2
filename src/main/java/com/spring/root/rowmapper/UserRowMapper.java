package com.spring.root.rowmapper;

import com.spring.root.domain.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by cormac on 11/23/16.
 */
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int index) throws SQLException {
        User user = new User();

        user.setID(rs.getInt("id"));
        user.setFirstName(rs.getString("firstName"));
        user.setLastName(rs.getString("lastName"));
        user.setPasswd(rs.getString("passwd"));
        user.setCreditLimit(rs.getDouble("c_Limit"));

        return user;
    }

}
