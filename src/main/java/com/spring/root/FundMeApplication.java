package com.spring.root;

import com.spring.root.dao.ProjectDao;
import com.spring.root.domain.Project;
import com.spring.root.domain.User;
import com.spring.root.repository.UserRepository;

import com.spring.root.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

@SpringBootApplication
public class FundMeApplication implements CommandLineRunner {

	@Autowired
	UserRepository userRepository;
	//@Autowired
	//UserService userService;

	@Autowired
	private ProjectService projectService;

	private User currentUser;
	private boolean loggedIn = false;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(FundMeApplication.class, args);

	}


	@Override
	public void run(String... strings) throws Exception {

		List<User> userList = this.userRepository.getAll();
        for(User user : userList){
            user.print();
        }

	}


	//Needs user service first
	//Takes username and password as string and checks if they match any users in users table
//	public boolean logIn(String username, String password){
//		List<User> users = userService.getAll();
//		for(User user: users){
//			if(user.getFullName().equals(username) && user.getPasswd().equals(password))
// 				currentUser = user;
//				loggedIn = true;
//				return loggedIn;
//		}
//		return loggedIn;
//	}


	//Logs out user, returns false to webpage
	public boolean logOut(){
		currentUser = null;
		loggedIn = false;
		return loggedIn;
	}

	//Takes all details from the webpage and creates a new project with them
	public void addProject(String projectName, String description, int goalLimit, int timeLimit){
		Project project = new Project();
		project.setProjectName(projectName);
		project.setUserID(currentUser.getID());
		project.setDescription(description);
		project.setGoalAmount(goalLimit);
		project.setTimeLimit(timeLimit);
		projectService.save(project);
	}

	//Returns a list of all projects
	public List<Project> getAllProjects(){
		return projectService.getAll();
	}

	//Takes current user logged in as parameter, returns all projects belonging to said user
	public List<Project> getAllCurrentUserProjects(){
		return projectService.getUsersProjects(currentUser);
	}

	//Only necessary if edit button is there on all project. If not this method is redundant
	public boolean canEdit(Project project){
		if(currentUser.getID() == project.getUserID()){
			return true;
		}
		else{
			return false;
		}
	}

	//Takes project selected for edit and new description as parameters.
	//Set description as the new description and saves the project
	public void editProject(Project project, String newDescription){
		project.setDescription(newDescription);
		projectService.save(project);
	}

}