package com.spring.root.domain;

/**
 * Created by Kamil on 01/12/2016.
 */
public class Pledge {


    private int pledgeID;
    private double amount;

    //private User user;
    //private Project project;

    public Pledge() {

        //this.user = new User();
        //this.project = new Project();

    }

    public int getPledgeID() {
        return pledgeID;
    }

    public void setPledgeID(int pledgeID) {
        this.pledgeID = pledgeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    //Create user and project setters and getters

    public String toString(){

        return "Pledge ID: " + pledgeID + "\n" + "Amount: " + amount;

    }

}
