package com.spring.root.domain;

/**
 * Created by cormac on 11/23/16.
 */
public class User {

    private int ID;

    private String firstName;

    private String lastName;

    private String passwd;

    private double creditLimit;



    
    public User(){
        this.setID(ID);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPasswd(passwd);
        this.setCreditLimit(creditLimit);
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }





    public int getID() {
        return ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName(){
        return this.getFirstName() + " " + this.getLastName();
    }

    public String getPasswd() {
        return passwd;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    @Override
    public String toString(){
        return "ID: " + ID + "\tFull Name:" + this.getFullName() + "\tPassword: " + passwd + "\tCredit Limit: " + creditLimit;
    }

    public void print(){
        System.out.println(toString());
    }
}
