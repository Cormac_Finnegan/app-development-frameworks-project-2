package com.spring.root.service;

import com.spring.root.domain.Project;
import com.spring.root.dao.ProjectDao;
import com.spring.root.domain.User;
import com.spring.root.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by seanc on 23/11/2016.
 */

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepo;

    @Override
    public Project get(int id) {
        return projectRepo.get(id);
    }

    @Override
    public void save(Project project) {
        projectRepo.save(project);
    }

    @Override
    public void remove(Project project) {
        projectRepo.remove(project);
    }

    @Override
    public List<Project> getAll() {
        return projectRepo.getAll();
    }

    @Override
    public List<Project> getUsersProjects(User user) {
        return projectRepo.getUserProjects(user.getID());
    }

}
