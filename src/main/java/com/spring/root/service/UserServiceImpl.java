package com.spring.root.service;

import com.spring.root.domain.User;
import com.spring.root.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by C.I.T on 07-Dec-16.
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User get(int id) {
        return userRepository.get(id);
    }
    @Override
    public void save(User user) {
        userRepository.save(user);
    }
    @Override
    public void add(User user) {
        userRepository.add(user);
    }
    @Override
    public void update(User user) {
        userRepository.update(user);
    }
    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }
}
