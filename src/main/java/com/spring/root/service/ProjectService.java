package com.spring.root.service;

import com.spring.root.domain.Project;
import com.spring.root.domain.User;

import java.util.List;

/**
 * Created by seanc on 23/11/2016.
 */
public interface ProjectService {

    public Project get(int id);
    public void save(Project project);
    public void remove(Project project);
    public List<Project> getAll();
    public List<Project> getUsersProjects(User user);


}
