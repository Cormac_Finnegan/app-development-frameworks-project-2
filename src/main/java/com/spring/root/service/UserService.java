package com.spring.root.service;

import com.spring.root.domain.User;

/**
 * Created by C.I.T on 07-Dec-16.
 */
public interface UserService {

    User get(int id);
    void save(User user);
    void add(User user);
    void update(User user);
    void delete(User user);
    void getAll();
}
