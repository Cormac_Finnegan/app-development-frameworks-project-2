package com.spring.root.repository;

import com.spring.root.domain.Project;

import java.util.List;

/**
 * Created by seanc on 23/11/2016.
 */
public interface ProjectRepository {

    public Project get(int id);
    public void save(Project project);
    public void remove(Project project);
    public List<Project> getAll();
    public List<Project> getUserProjects(int userID);

}
