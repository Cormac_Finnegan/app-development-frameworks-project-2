package com.spring.root.repository;

import com.spring.root.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cormac on 11/23/16.
 */
@Repository
public interface UserRepository {

    User get(int id);

    void save(User user);

    void add(User user);

    void update(User user);

    void delete(User user);

    List<User> getAll();
}
