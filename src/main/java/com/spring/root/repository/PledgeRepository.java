package com.spring.root.repository;

import com.spring.root.domain.Pledge;

import java.util.List;

/**
 * Created by Kamil on 01/12/2016.
 */
public interface PledgeRepository {

    public Pledge get(int pledgeId);
    public void save(Pledge pledge);
    public void remove(Pledge pledge);
    public List<Pledge> getAll();

}
