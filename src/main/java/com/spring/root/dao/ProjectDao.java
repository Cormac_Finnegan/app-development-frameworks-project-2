package com.spring.root.dao;

import com.spring.root.domain.Project;
import com.spring.root.repository.ProjectRepository;
import com.spring.root.rowmapper.ProjectRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by seanc on 23/11/2016.
 */

@Repository
public class ProjectDao implements ProjectRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Project get(int id) {
        // TODO Auto-generated method stub
        String sql = "SELECT * FROM projects WHERE projectID = ?";
        Project project = jdbcTemplate.queryForObject(sql, new Object[]{id}, new ProjectRowMapper());
        return project;
    }

    @Override
    public void save(Project project) {
        // TODO Auto-generated method stub
        if(project.getProjectID() != 0){
            update(project);
        }
        else {
            add(project);
        }
    }

    private void update(Project project){
        String sql = "UPDATE projects SET userID = ?, projectName = ?, description = ?, goalAmount = ?, timeLimit = ? WHERE projectID = ?";
        jdbcTemplate.update(sql, new Object[]{ project.getUserID(), project.getProjectName(), project.getDescription(),
                project.getGoalAmount(), project.getTimeLimit(), project.getProjectID()});
    }

    private void add(Project project){
        String sql = "INSERT INTO projects(projectID, userID, projectName, description, goalLimit, timeLimit) VALUES (?,?,?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{project.getProjectID(), project.getUserID(), project.getProjectName(),
                project.getDescription(), project.getGoalAmount(), project.getTimeLimit()});
    }

    @Override
    public void remove(Project project) {
        String sql = "DELETE projects WHERE projectID = ?";
        jdbcTemplate.update(sql, new Object[]{project.getProjectID()});
    }

    @Override
    public List<Project> getAll() {

        return jdbcTemplate.query("SELECT * FROM projects", new ProjectRowMapper());
    }

    @Override
    public List<Project> getUserProjects(int userID) {
        return jdbcTemplate.query("SELECT * FROM projects WHERE userID = ?", new ProjectRowMapper(), new Object[]{userID});
    }

}
