package com.spring.root.dao;

import com.spring.root.domain.User;
import com.spring.root.repository.UserRepository;
import com.spring.root.rowmapper.UserRowMapper;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cormac on 11/23/16.
 */
@Repository
public class UserDao implements UserRepository{

    private JdbcTemplate jdbcTemplate;

    //Singleton
    @Autowired
    public UserDao(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User get(int id) {
        String sql = "SELECT * FROM users WHERE id = ?";
        User user = jdbcTemplate.queryForObject(sql, new Object[] { 1 }, new UserRowMapper());
        return user;
    }

    @Override
    public void save(User user) {
        if (user.getID() != 0) {
            update(user);
        } else {
            add(user);
        }
    }

    private void add(User user) {
        String sql = "INSERT INTO users (firstName, lastname, gender) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, new Object[] { user.getFirstName(), user.getLastName(), user.getCreditLimit()} );
    }

    private void update(User user) {
        String sql = "UPDATE users SET firstName = ?, lastName = ?, gender = ? WHERE id = ?";
        jdbcTemplate.update(sql, new Object[] { user.getFirstName(), user.getLastName(),
                user.getCreditLimit(), user.getID()} );
    }

    @Override
    public void delete(User user) {
        String sql = "DELETE users WHERE id = ?";
        jdbcTemplate.update(sql, new Object[] { user.getID() } );
    }

    @Override
    public List<User> getAll() {
        String sql = "SELECT * FROM users";
        return jdbcTemplate.query(sql, new UserRowMapper());
    }
}
