CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  firstName varchar(100) NOT NULL,
  lastName varchar(100) NOT NULL,
  passwd varchar(128) NOT NULL,
  c_limit double NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE projects (
  projectID int NOT NULL AUTO_INCREMENT,
  userID int,
  projectName Text NOT NULL,
  description Text,
  goalAmount int,
  timeLimit int,
  PRIMARY KEY(projectID),
  FOREIGN KEY(userID)
);

CREATE TABLE pledge (
  pledgeId int(11) NOT NULL AUTO_INCREMENT,
  userId int(11) NOT NULL AUTO_INCREMENT,
  projectID int(11) NOT NULL AUTO_INCREMENT,
  amonut double  NOT NULL,
  PRIMARY KEY (pledgeId)
);